# rEFInd theme Irregular

A bring-your-own-background-image refind theme.

NOTE: this is a fork of bobafetthotmail's Regular theme which in turn is a fork of munlik's Regular theme and it was started to recreate the look and feel of realmain's Sunset rEFInd Theme. So most of the stuff here is not mine.

Background image not provided, use your own!

![Screenshot 01](branding/screenshot_002.jpg)

![Screenshot 02](branding/screenshot_001.jpg)

**press F10 to take screenshot**

Feel free to share your setup!

### Installation:

Obviously this only works if you already have refind installed. For more information check the official [rEFInd](http://www.rodsbooks.com/refind/) website.

1. Download or clone the theme source from github

1. Run the install script with root privileges. It will prompt you for a
   png wallpaper location and a few other things.

   ```
   sudo ./install.sh
   ```
