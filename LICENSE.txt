The content of this repository is under mixed licenses:

 * The provided Noto Mono Regular font is licensed under the SIL open font license version 1.1 from Google (http://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL). 

 * The font2png.sh script is taken from the rEFInd project and is licensed under GPLv3 by Roderick W. Smith with contributions from munlik. The changes have been submitted upstream for inclusion.
 
 * The render_bitmap and install script is taken from munlik's Refind-Regular under an unspecified license and adapted by Alexander Hedges.
 
 * The OS icons are probably taken from all over the place and some made by munlik himself. This probably constitutes fair use, or the artwork of the open source programs is also open source.
 
 * The picture used in the screenshot has been taken from Wallhaven where it has probably been uploaded by a third party without a declaration of origin.

 * Everything else is licensed under the MIT License: Copyright 2019 Alexander Hedges
