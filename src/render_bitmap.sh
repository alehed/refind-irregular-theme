#!/usr/bin/env bash

INKSCAPE=`which inkscape 2> /dev/null`
OPTIPNG=`which optipng 2> /dev/null`

SCALE=1
SRC_DIR="$(dirname $0)/svg/"
# Inkscape has a default of 96 pixels per inch since version 92 (it used to be 90)
DPI=96

function print_help(){
    echo "Render the SVG icons to PNG at the given scale"
    echo "Usage:"
    echo "$0 [[-s|--scale] <scale_factor>] <outdir>"
    echo "or"
    echo "$0 [-h|--help]"
    echo ""
    echo "Options:"
    echo "-s,--scale: <number>      A number indicating the scaling factor (defaults to 1)"
    echo ""
    echo "-h,--help                 Display this help message and exit"
    echo ""
    exit 1
}


if [ $# -ne 0 ]
then
    ARGS=`getopt -a -o hs: -l help,scale: -n "$0" -- "$@"`
    eval set -- "$ARGS"

    while [ $# -ne 0 ]
    do
        case "$1" in
            -h|--help)
                print_help
                exit 1
                ;;
            -s|--scale)
                SCALE=$2
                shift 2
                ;;
            --)
                shift
                break
                ;;
        esac
    done
else
    echo "Try \`$0 --help' for more information." 1>&2
    exit 1
fi

if ! [[ $SCALE =~ ^[1-4]$ ]]
then
    echo "$SCALE is not a valid size (has to be between 1 and 4, inclusive)."
    exit 1
fi

#output directory
if [ $# -gt 0 ] && [ -d "$1" ]
then
    OUT_DIR=$1
    shift 1
else
    echo "$0 Output directory must be specified." 1>&2
    exit 1
fi

function render_bitmap(){
    for svgfile in $(ls "$SRC_DIR"/*.svg)
    do
        filename=`basename "$svgfile" '.svg'`
        if [ -f "$OUT_DIR/$filename.png" ]
        then
            echo "'$OUT_DIR/$filename.png' already exists"
        else
            echo -n "Creating $OUT_DIR/$filename.png ..."
        	$INKSCAPE --export-area-page \
                      --export-dpi=$(($SCALE * $DPI)) \
                      --export-png="$OUT_DIR/$filename.png" \
                      "$svgfile" \
                      2&> /dev/null \
            &&
            if [[ -x $OPTIPNG ]]
            then
                $OPTIPNG -o7 --quiet "$OUT_DIR/$filename.png"
            fi
            echo " - [DONE]"
        fi
    done
}

render_bitmap
