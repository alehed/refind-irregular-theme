with import <nixpkgs> {}; {
  pythonEnv = stdenv.mkDerivation {
    name = "image-generation-env-basic";
    buildInputs = [
      inkscape
      optipng
      imagemagick
    ];
  };
}
