#!/bin/sh

# An installer for refind-theme-regular by Munlik, adapted by Alexander Hedges

script_loc=$(dirname $0)

#Useful formatting tags
bold=$(tput bold)
normal=$(tput sgr0)

#Check if root
[[ $EUID -ne 0 ]] && echo "This script must be run as root." && exit 1

#Set install path
echo "Enter rEFInd install location"
read -e -p "Default - ${bold}/boot/efi/EFI/refind/${normal}: " refind_loc
if test -z "$refind_loc";
then
    refind_loc="/boot/efi/EFI/refind/"
fi
if test "${refind_loc: -1}" != "/"
then
    refind_loc="$refind_loc/"
fi
location="$refind_loc"themes/refind-theme-irregular/

# Remove previous installs
echo -n "Deleting older installed versions (if any)"
if [ -e "$location" ]
then
    rm -rf "$location"
fi
mkdir "$location"
echo " - [DONE]"

#Set icon size
echo "Pick an icon scale: (larger icons look better on bigger and denser displays)"
while true
do
    read -p "${bold}1: small (128px)${normal}, 2: medium (256px), 3: large (384px), 4: extra-large (512px): " scale
    if test -z "$scale";
    then
        scale=1
    fi
    if [ "$scale" -gt 4 ] || [ "$scale" -lt 1 ]
    then
        echo "Incorrect choice."
    else
        break
    fi
done
size_big=$((128 * "$scale"))
size_small=$((48 * "$scale"))

# Get background picture
echo "Pick a background picture to use."
echo "Make sure it has your screen resolution and is in the PNG format."
while true
do
    read -p "Enter the path to the picture: " picture_loc
    if [ -f "$picture_loc" ] && echo "$picture_loc" | grep ".png$" &> /dev/null
    then
        break
    else
        echo "Incorrect choice."
    fi
done

#Set theme color
echo "Select a theme color"
while true
do
    read -p "${bold}1: light${normal}, 2: dark: " theme_select
    if test -z "$theme_select";
    then
        theme_select=1
    fi
    case "$theme_select" in
        1)
            font_color="black"
            theme_path=""
            break
            ;;
        2)
            font_color="white"
            theme_path="_dark"
            break
            ;;
        *)
            echo "Incorrect choice."
            ;;
    esac
done

# Move background picture
echo -n "Moving background picture"
cp "$picture_loc" "$location"background.png
echo " - [DONE]"

# Generate bitmaps
echo "Generating png icons in the specified scale:"
mkdir -p "$location"icons/
$script_loc/src/render_bitmap.sh --scale "$scale" "$location"icons/

# Generate font
echo "Generating rEFInd readable font:"
mkdir -p "$location"fonts/
font_name="NotoSansMono-Regular"
font_size=14
$script_loc/src/font2png.sh -f "$script_loc"/src/fonts/"$font_name.ttf" -s $(("$font_size" * "$scale")) -c "$font_color" "$location"fonts/"$font_name-$font_size.png"

#Uncomment relevant lines from src/theme.conf
echo -n "Generating theme file theme.conf"
cp "$script_loc/src/theme.conf" "$location"theme.conf
sed -i "s/#big_icon_size $size_big/big_icon_size $size_big/" "$location"theme.conf
sed -i "s/#small_icon_size $size_small/small_icon_size $size_small/" "$location"theme.conf
sed -i "s/#selection_big themes\/refind-theme-irregular\/icons\/selection$theme_path-big.png/selection_big themes\/refind-theme-irregular\/icons\/selection$theme_path-big.png/" "$location"theme.conf
sed -i "s/#selection_small themes\/refind-theme-irregular\/icons\/selection$theme_path-small.png/selection_small themes\/refind-theme-irregular\/icons\/selection$theme_path-small.png/" "$location"theme.conf
echo "font themes/refind-theme-irregular/fonts/$font_name-$font_size.png" >> "$location"theme.conf
echo " - [DONE]"

# Edit refind.conf - remove older themes
echo -n "Removing old themes from refind.conf"
sed --in-place=".bak" 's/^\s*include/# (disabled) include/' "$refind_loc"refind.conf
echo " - [DONE]"

# Edit refind.conf - add new theme
echo -n "Updating refind.conf"
echo "
include themes/refind-theme-irregular/theme.conf" | tee -a "$refind_loc"refind.conf &> /dev/null
echo " - [DONE]"

echo "Thank you for installing rEFInd theme Irregular."
echo "NOTE: If your not geting your full resolution or have color issues then try disabling the CSM in your UEFI settings."
